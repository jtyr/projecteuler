#!/usr/bin/perl

###
#
# Description:
#
###
#
# The sum of the squares of the first ten natural numbers is,
#
# 1^2 + 2^2 + ... + 10^2 = 385
#
# The square of the sum of the first ten natural numbers is,
#
# (1 + 2 + ... + 10)^2 = 55^2 = 3025
#
# Hence the difference between the sum of the squares of the first ten natural
# numbers and the square of the sum is 3025  385 = 2640.
#
# Find the difference between the sum of the squares of the first one hundred
# natural numbers and the square of the sum.
#
###

use strict;
use warnings;

my $max = 100;

my $sun_square = 0;
my $square_sum = 0;

for (my $i=1; $i<=$max; $i++) {
	$sun_square += $i**2;
	$square_sum += $i;
}

printf "Diff: %d\n", $square_sum**2 - $sun_square;

exit 0;
