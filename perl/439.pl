#!/usr/bin/perl

###
#
# Description:
#
###
#
# Let d(k) be the sum of all divisors of k.
# We define the function S(N) = Sum{1<i<N} Sum{1<j<N} d(ij).
# For example, S(3) = d(1) + d(2) + d(3) + d(2) + d(4) + d(6) + d(3) + d(6) + d(9) = 59.
#
# You are given that S(10^3) = 563576517282 and S(10^5) mod 10^9 = 215766508.
# Find S(10^11) mod 10^9.
#
###

use strict;
use warnings;

my $n = 10000;#10**11;
my $sum = 0;

my %div;

for (my $i=1; $i<=$n; $i++) {
	for (my $j=1; $j<=$n; $j++) {
		my $tmp = $i*$j;

		$sum += $tmp;
=cut
		if (exists $div{$tmp}) {
			$sum += $div{$tmp};
		} else {
			my $tmp_sum = 0;
			for (my $k=1; $k<=$tmp; $k++) {
				unless ($tmp % $k) {
					$tmp_sum++;
#					$sum++;
				}
			}
			$div{$tmp} = $tmp_sum;
			$sum += $tmp_sum;
		}
=cut
	}

	printf "%d: %d\n", $i, $sum;
}

printf "\nSum: %d\n", $sum % 10**9;

exit 0;
