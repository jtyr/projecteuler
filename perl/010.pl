#!/usr/bin/perl

###
#
# Description:
#
###
#
# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
#
# Find the sum of all the primes below two million.
#
###

use strict;
use warnings;

my $max = 2*1000*1000;
my $n = 1;
my $sum = 0;

while ($n < $max) {
	$n++;

	my $failed = 0;

	for (my $i=2; $i<int($n/2)+1; $i++) {
		if ($n % $i == 0) {
			$failed = 1;
			last;
		}
	}

	unless ($failed) {
		$sum += $n;
	}
}

printf "Sum: %d\n", $sum;

exit 0;
