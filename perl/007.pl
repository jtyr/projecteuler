#!/usr/bin/perl

###
#
# Description:
#
###
#
# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
# that the 6th prime is 13.
#
# What is the 10001st prime number?
#
###

use strict;
use warnings;

my $max = 10001;
my $n = 0;

while ($max > -1) {
	$n++;

	my $failed = 0;

	for (my $i=2; $i<int($n/2)+1; $i++) {
		if ($n % $i == 0) {
			$failed = 1;
			last;
		}
	}

	unless ($failed) {
		$max--;
	}
}

printf "Prime num: %d\n", $n;

exit 0;
