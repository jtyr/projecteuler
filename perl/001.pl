#!/usr/bin/perl

###
#
# Description:
#
###
#
# If we list all the natural numbers below 10 that are multiples of 3 or 5, we
# get 3, 5, 6 and 9. The sum of these multiples is 23.
#
# Find the sum of all the multiples of 3 or 5 below 1000.
#
###

use strict;
use warnings;

my $max = 1000;
my $sum = 0;

for (my $n=1; $n<$max; $n++) {
	unless ($n % 3 and $n % 5) {
		$sum += $n;
	}
}

printf "Sum: %d\n", $sum;

exit 0;
