#!/usr/bin/perl

###
#
# Description:
#
###
#
# A Pythagorean triplet is a set of three natural numbers, a  b  c, for which,
#
# a^2 + b^2 = c^2
# For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
#
# There exists exactly one Pythagorean triplet for which a + b + c = 1000.
# Find the product abc.
#
###

use strict;
use warnings;

my ($a, $b);
my $c = 2;
my $max = 1000;
my $found = 0;

while (1) {
	$c++;

	for ($b=1; $b<$c; $b++) {
		for ($a=1; $a<$b; $a++) {
			if ($a + $b + $c > $max) {
				last;
			} elsif ($a**2 + $b**2 == $c**2 and $a + $b + $c == $max) {
				$found = 1;
				last;
			}
		}

		last if ($found or $a > $max or $b > $max);
	}

	last if ($found or $c > $max);
}

if ($found) {
	printf "Tripplet product: %d\n", $a * $b * $c;
} else {
	print 'Not found!'."\n";
}

exit 0;
