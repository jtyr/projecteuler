#!/usr/bin/perl

###
#
# Description:
#
###
#
# 2520 is the smallest number that can be divided by each of the numbers from 1
# to 10 without any remainder.
#
# What is the smallest positive number that is evenly divisible by all of the
# numbers from 1 to 20?
#
###

use strict;
use warnings;

my $min = 1;
my $max = 20;

my $n = ($max-1)*$max;

while (1) {
	my $failed = 0;

	for (my $i=$min; $i<=$max; $i++) {
		if ($n % $i) {
			$failed = 1;
			last;
		}
	}

	if ($failed) {
		$n += $max;
	} else {
		last;
	}
}

printf "Number: %d\n", $n;

exit 0;
