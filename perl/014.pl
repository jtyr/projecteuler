#!/usr/bin/perl

###
#
# Description:
#
###
#
# The following iterative sequence is defined for the set of positive integers:
#
# n -> n/2 (n is even)
# n -> 3n + 1 (n is odd)
#
# Using the rule above and starting with 13, we generate the following sequence:
#
# 13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1
#
# It can be seen that this sequence (starting at 13 and finishing at 1) contains
# 10 terms. Although it has not been proved yet (Collatz Problem), it is thought
# that all starting numbers finish at 1.
#
# Which starting number, under one million, produces the longest chain?
#
# NOTE: Once the chain starts the terms are allowed to go above one million.
#
###

use strict;
use warnings;

my $num = 1000*1000-1;
my $max = 0;
my $max_num = 0;

for (my $i=$num; $i>1; $i--) {
	my $n = $i;
	my $tmp_max = 0;

	while ($n > 1) {
		unless ($n % 2) {
			# even
			$n = $n/2;
		} else {
			# odd
			$n = 3*$n + 1;
		}

		$tmp_max++;
	}

	if ($max < $tmp_max) {
		$max = $tmp_max;
		$max_num = $i;
	}
}

printf "Max: %d\n", $max_num;

exit 0;
