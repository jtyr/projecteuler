#!/usr/bin/perl

###
#
# Description:
#
###
#
# A palindromic number reads the same both ways. The largest palindrome made
# from the product of two 2-digit numbers is 9009 = 91 x 99.
#
# Find the largest palindrome made from the product of two 3-digit numbers.
#
###

use strict;
use warnings;

my $palindrome = 0;
my $max = 999;

for (my $i=$max; $i>=int($max/2); $i--) {
	for (my $j=$max; $j>int($max/2); $j--) {
		my $n = $i*$j;

		my $len = length $n;
		my $len_half = int $len/2;

		if (substr($n, 0, $len_half) eq reverse(substr($n, $len-$len_half, $len_half))) {
			if ($n > $palindrome) {
				$palindrome = $n;
			}

			last;
		}
	}
}

printf "Palindrome: %d\n", $palindrome;

exit 0;
