#!/usr/bin/perl

###
#
# Description:
#
###
#
# The prime factors of 13195 are 5, 7, 13 and 29.
#
# What is the largest prime factor of the number 600851475143 ?
#
###

use strict;
use warnings;

my $max = 600851475143;

for (my $i=2; $i<$max; $i++) {
	unless ($max % $i) {
		$max = int $max / $i;
	}
}

printf "Max: %d\n", $max;


exit 0;
