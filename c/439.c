/**
*
* Description:
*
***
*
* Let d(k) be the sum of all divisors of k.
* We define the function S(N) = Sum{1<i<N} Sum{1<j<N} d(ij).
* For example, S(3) = d(1) + d(2) + d(3) + d(2) + d(4) + d(6) + d(3) + d(6) + d(9) = 59.
*
* You are given that S(10^3) = 563576517282 and S(10^5) mod 10^9 = 215766508.
* Find S(10^11) mod 10^9.
*
**/

#include <stdio.h>
#include <stdlib.h>

void main() {
	unsigned long long int n = 100000000000;
	unsigned long long int sum = 0;
	unsigned long long int i = 1;
	unsigned long long int j = 1;
	unsigned long long int k = 1;
	unsigned long long int tmp = 1;

	for (i=1; i<=n; i++) {
		for (j=1; j<=n; j++) {
			tmp = i*j;

			sum += tmp;

			for (k=1; k<=tmp; k++) {
				if (tmp % k == 0) {
					sum++;
				}
			}
		}

		printf("%llu: %llu\r", i, sum);
	}

	printf("\nSum: %llu\n", sum % 1000000000);
}
