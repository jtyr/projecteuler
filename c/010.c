/**
*
* Description:
*
***
*
* The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
*
* Find the sum of all the primes below two million.
*
***/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void main() {
	int max = 2*1000*1000;
	int n = 1;
	long sum = 0;
	int i;
	int failed;

	while (n < max) {
		n++;

		failed = 0;

		for (i=2; i<floor(n/2)+1; i++) {
			if (n % i == 0) {
				failed = 1;
				break;
			}
		}

		if (failed == 0) {
			sum += n;
		}
	}

	printf("Sum: %ld\n", sum);
}
