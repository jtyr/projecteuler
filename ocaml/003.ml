#!/usr/bin/ocaml

(**
*
* Description:
*
***
*
* The prime factors of 13195 are 5, 7, 13 and 29.
*
* What is the largest prime factor of the number 600851475143 ?
*
***)

open Format;;

let max = ref 600851475143;;

for i = 2 to !max do
	if !max mod i <> 0 then
		max := int_of_float(floor(float_of_int !max /. float_of_int i));
done;;

printf "Max: %d\n" !max;;

exit 0
