#!/usr/bin/ocaml

(**
*
* Description:
*
***
*
* 2520 is the smallest number that can be divided by each of the numbers from 1
* to 10 without any remainder.

* What is the smallest positive number that is evenly divisible by all of the
* numbers from 1 to 20?
*
***)

open Format;;

let min = 1;;
let max = 20;;

let n = ref ((max - 1) * max);;
let failed = ref 0;;

while !failed <> max do
	failed := 0;

	for i = min to max do
		if !n mod i = 0 then
			failed := !failed + 1;
	done;

	if !failed <> max then
		n := !n + max;
done;;

printf "Number: %d\n" !n;;

exit 0
