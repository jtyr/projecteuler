#!/usr/bin/ocaml

(**
*
* Description:
*
***
*
* A palindromic number reads the same both ways. The largest palindrome made
* from the product of two 2-digit numbers is 9009 = 91 x 99.
*
* Find the largest palindrome made from the product of two 3-digit numbers.
*
***)

open Format;;

let palindrome = ref 0;;
let max = 999;;
let n = ref 0;;
let len = ref 0;;
let len_half = ref 0;;

(* From: http://rosettacode.org/wiki/Reverse_a_string#OCaml *)
let rev_string str =
  let len = String.length str in
  let res = String.create len in
  let last = len - 1 in
  for i = 0 to last do
    let j = last - i in
    res.[i] <- str.[j];
  done;
  (res);;

for i = max downto int_of_float(floor(float_of_int max /. float_of_int 2)) do
	for j = max downto int_of_float(floor(float_of_int max /. float_of_int 2)) do
		n := i*j;

		len := String.length (string_of_int !n);
		len_half := int_of_float(floor(float_of_int !len /. float_of_int 2));

		if String.sub (string_of_int !n) 0 !len_half = rev_string (String.sub (string_of_int !n) (!len - !len_half) !len_half) then
			if !n > !palindrome then
				palindrome := !n;
	done;
done;;

printf "Palindrome: %d\n" !palindrome;;

exit 0
