#!/usr/bin/ocaml

(**
*
* Description:
*
***
*
* If we list all the natural numbers below 10 that are multiples of 3 or 5, we
* get 3, 5, 6 and 9. The sum of these multiples is 23.
*
* Find the sum of all the multiples of 3 or 5 below 1000.
*
***)

open Format;;

let max = 1000;;
let sum = ref 0;;

for n = 1 to max do
	if n mod 3 = 0 or n mod 5 = 0 then begin
		sum := !sum + n;
	end;
done;;

printf "Sum: %d\n" !sum;;

exit 0
